from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21500

class servidorNutricion(BaseHTTPRequestHandler):
    """Un servidor básico
    Cuidado: esta clase está sin terminar y se incluye sólo como modelo.
    """
    def do_GET(self):
        """
        Este metodo gestiona las peticiones GET de HTTP
        """
        self.send_response(200)
        self.send_header('Content-type','text/html; charset=utf-8')
        self.end_headers()
        self.wfile.write("Hello World!".encode("utf-8"))
        return
    
if __name__ == "__main__":
    try:
        server = HTTPServer(('', PORT_NUMBER), servidorNutricion)
        print('Started server on port ' , PORT_NUMBER)
        server.serve_forever()

    except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()
