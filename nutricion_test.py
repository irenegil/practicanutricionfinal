import unittest
from http.server import HTTPServer
from nutricion_servidor import servidorNutricion
import threading
import time
import requests

IP = "127.0.0.1"
PORT = 12345

class TestSequence(unittest.TestCase):

    def test_request01(self):
        # start our server
        server = HTTPServer((IP, PORT), servidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
             response = requests.get("http://localhost:12345/")
             html = response.text
             self.assertEqual(html, 'Hello World!')
        finally:
             # and shutdown the server
             server.shutdown()
             server.server_close()

    def test_request02(self):
        """
          Método utilizado para comprobar el código que me devuelve la petición, 
          no el contenido de ella. De esta forma hago petición y compruebo
          si me devuelve un 200 de forma correcta.
        """
        server = HTTPServer((IP, PORT), servidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
             response = requests.get("http://localhost:12345/")
             status_code = response.status_code 
             self.assertEqual(status_code, 200)
        finally:
             # and shutdown the server
             server.shutdown()
             server.server_close()

if __name__ == "__main__":
    unittest.main()
