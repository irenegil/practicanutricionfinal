# Practica Servidor nutrición

Una práctica para la asignatura PER usando conceptos basados en nutrición y servidores web.

Entrega el día 24 de Mayo con examen el 24 de Mayo.

## Objetivo


Se trata de hacer un servidor web en Python que permita gestionar dietas, alimentos y nutrientes.
Adicionalmente al servidor web, el alumno deberá desarrollar su propia batería de tests unitarios. En la corrección de la práctica se evaluará el correcto desarrollo de ambos ficheros. Es decir, la nota se pondrá en función de la calidad del código del servidor, así como el número y calidad de los tests realizados.


## Documentación

La práctica deberá tener un documento que resuma el funcionamiento del código. Además, deberá explicar los siguientes conceptos teóricos y su utilización en la práctica:

- Estructura de una URL y las URL utilizadas en la práctica
- Diferencia entre HTML y JSON
- Cabeceras HTTP. Qué cabeceras se utilizan en la práctica
- ¿Qué es una API REST?
- Verbos HTTP. Qué es un verbo HTTP y qué verbos se utilizan en la práctica y dónde.

## Pasos previos a la práctica
Debe crear un proyecto privado en su cuenta de gitlab con el nombre de **"PER24-pfinal-Nutricion"**. **Es importante que mantenga ese nombre** ya que se utilizará para la corrección de la práctica. Este proyecto deberá ser diferente al de las prácticas anteriores. Aunque se puede reutilizar código que el alumno tenga de otras prácticas, el proyecto para la práctica final debe ser creado desde cero.
El proyecto debe **ser privado y no visible** a ningún compañero, pero deben dar acceso como mínimo con el role de "REPORTER" a los tres (3) profesores para su posterior evaluación(Álvaro Ruiz Calzada, Agustín Santos Méndez y Rubén Álvarez Martín). Se pasarán comprobaciones para detectar la copia de código. Si se detecta que el proyecto contiene código y no es privado, el alumno **suspenderá automáticamente** la práctica.

Dentro de ese proyecto debe haber al menos tres ficheros llamados:
 -  "servidor.py": código del servidor en python.
 -  "test_servidor.py": test utilizado por el alumno para comprobar el funcionamiento del servidor.
 -  "nutricion.json": tendrá la información en formato json que obtendrá el servidor.

En el proyecto podría haber otros ficheros adicionales si fuera necesario.

## Comportamiento del servidor web
El servidor web deberá cumplir los siguientes requisitos:

- El servidor deberá heredar de "BaseHTTPRequestHandler" y llamarse "ServidorNutricion".
- Debe obtener los datos del fichero JSON "nutricion.json".
- Deberá correr en la IP local (127.0.0.1) y en el puerto 21500.
- Debe servir las páginas (url) siguientes:
	* "/", "/index.html", "/index.htm". Aunque son tres url diferentes, se trata de la misma página de bienvenida. Dicha página debe tener un formulario en el que se pidan varios datos que permitirán consultar un día de la semana y determinada comida. Para ello, debe introducir por pantalla en un formulario:
      - El día de la semana. 
      - La comida que quiero consultar (desayuno, comida, merienda, cena...)
    * Dicho formulario debe tener un botón para hacer el submit llamado "Consultar". Al pulsar ese botón del formulario:
      - Se debe de enviar con el método GET los datos del día y de la comida a la página "/consulta".
      - Debe devolver los datos de alimentos de los datos consultados. 
      - En el caso de que no se introduzca nada en el campo de la comida debe devolver el día entero.

	* "/consulta". Se debe enviar con el método GET con un día de la semana y un tipo de comida. 
	  Cuando se reciba dicha petición se debe:
    		- Extraer un día de la semana y el tipo de comida 
    		- Debe mostrar en pantalla los alimentos de ese día de la semana y de esa comida. 
    		- Debe poderse enviar en el GET únicamente el día de la semana; es decir, que la comida sea opcional. En tal caso mostrará todas las comidas relativas a ese día. Se valorará positivamente que se muestre en pantalla en formato tabla elaborado con HTML.

	*  "/alimento/{id_alimento}". Debe devolver los nutrientes del alimento introducido en la url. Siendo {id_alimento} cualquier nombre de un alimento. Es decir, si introduzo "/alimento/manzana" y ese alimento se encuentra en el JSON, debe devolver los nutrientes relativos a ese alimento.
  
	* "/autor". Es una página HTML con la información de vuestro nombre y cuenta del gitlab

- No debe parar salvo que el usuario lo interrumpa mediante un Control-C. Es decir, debe servir páginas de forma indefinida.
- Debe ser robusto ante posibles fallos. Es decir, si el usuario pide una página que no existe o cualquier situación anómala, debe indicar tal situación y no parar.
- Si el usuario pide una página que no exista el servidor deberá indicar "Página no encontrada." Observe que la respuesta tiene una vocal acentuada. Se deberá utilizar el código de estado correspondiente (4XX).

> NOTA: Cuando el enunciado se refiere a **"comida"** se refiere a las distintas comidas que tiene el día y **"alimento"** al elemento que contiene los valores nutricionales.

## Extracción de los datos de dietas
Los datos de la dieta se van a obtener de un fichero JSON elaborado previamente. Este fichero va a contener los datos de una dieta semanal, así como sus alimentos y sus respectivos nutrientes. El servidor va a leer este fichero JSON y a obtener los datos que hayamos solicitado en cada consulta. 
Al igual que la práctica anterior, el alimento debería incluir unos nutrientes y una categoría de alimentos. Así que es necesario que incluya los siguientes elementos: 
- Día/fecha
- Comidas del día 
- Alimentos
- Nutrientes
- Categoría de alimento
  
A continuación se muestra el código de un posible fichero JSON. Se incluye como referencia orientativa, pero no es correcto ni conforme al enunciado de la práctica.

> Este  ejemplo es orientativo
```json
{
    "lunes": {
        "desayuno": {
          "tostada":{
            "nutrientes": {
              "hidratos": 3,
              "grasas": 1,
              "proteinas": 1
            },
            "categoria": "cereales"
          },
          "cafe":{
            "hidratos": 0,
            "grasas": 0.3,
            "proteinas": 0.1
          },
          "categoria": "bebida"
        },
        "comida": {
          "lentejas":{
            "nutrientes":{
              "hidratos": 5,
              "grasas": 2,
              "proteinas": 1
            },
            "categoria": "legumbres"
          }
        }
    }
}

```

## Ejemplo de formulario
A continuación se muestra el código de un posible formulario. Se incluye como referencia orientativa pero no es correcto ni conforme al enunciado la práctica. Se deja al alumno que detecte y corrija los cambios u errores que considere necesarios.
> El nombre de los campos o el funcionamiento de los botones no se corresponde con el enunciado.

```html
<!DOCTYPE html>
<html>
<body>

<h2>Ejemplo orientativo del formulario</h2>

<form action="/consulta">
  <label for="dia">Dia de la semana:</label><br>
  <input type="text" id="xx" name="xx" value="lunes"><br>
  <label for="comida">Comida:</label><br>
  <input type="text" id="yy" name="yy" value="desayuno"><br><br>
  <input type="submit" value="json">
</form> 

<p>Si quiere ver al autor de la práctica puede pulsar en  <a href="/autor">Autor</a>.</p>

<p>NOTA 1: Este formulario es orientativo y el alumno debe maquetar el HTML según le parezca conveniente</p>
<p>NOTA 2: El formulario contiene errores y no es conforme al enunciado</p>
</body>
</html>
```

## Extras 

Se valorarán los siguientes extras. Se puede hacer uno o varios de los extras propuestos. No será necesario realizar pruebas unitarias de los extras, aunque se valorarán positivamente. 
1. El servidor soportará dos formularios, uno que funciona sobre GET y otro sobre POST. El método POST puede agregar un alimento nuevo (con sus respectivos nutrientes y su tipo de alimento) a un día de la semana y a determinada comida del día. Ten en cuenta que para ello el formulario debería incluir los campos para nutrientes y categoria de alimento. (+2pt)
2. Se pueden obtener los datos de los alimentos haciendo la consulta a una API REST. Para ello se pueden consultar en los siguiente enlaces: 
   - https://openfoodfacts.github.io/openfoodfacts-server/api/ref-v3/#get-/api/v3/product/-barcode-
   - https://fdc.nal.usda.gov/api-guide.html
  
   Haciendo una petición a cualquiera de las APIs, podemos rellenar los datos de los nutrientes de determinado alimento y usarlo para nuestro servidor. 
   Esta funcionalidad se debe implementar en un **nuevo endpoint** que sea /consulta/v2, de tal forma que sea independiente a la funcionalidad básica y cuando se llame a este endpoint haga la petición a la API REST y muestre el resultado en pantalla. Esto es adicional a la funcionalidad extra, se deberá implementar **un nuevo formulario** que acepte un campo "alimento" y con este valor se haga la petición a la API Rest y muestre en pantalla la respuesta. (+2pt)
3. Se puede usar, además del fichero json, otro tipo de formato en el que residan los datos. Por ejemplo yaml, xml o html y con un desplegable en el formulario pueda elegir que fichero quiero emplear, si el json inicial o el extra implementado. (+1pt)
   

## Valoraciones

Se valoraran los siguientes puntos: 
- Correcta denominación de variables, métodos y funciones acorde a lo establecido por python: [Convenio de denominación](https://peps.python.org/pep-0008/#naming-conventions). 
- Uso de comentarios y documentación dentro del código: [Documentación](https://peps.python.org/pep-0008/#comments).
- Se valorará positivamente un mensaje coherente en los commits de git. Es decir, si un commit incluye una petición POST en nuestro servidor el commit debería ser lo suficientemente descriptivo para indicarlo (por ejemplo: "añadida petición POST /alimentos en el servidor").
- Se valorará positivamente el uso de excepciones en el código.
- Se valorará positivamente el correcto uso de la sintaxis de markdown en la documentación y respuestas de preguntas teóricas.

## Para recuperar este proyecto
Recuerda que para obtener una copia del proyecto puedes realizar el siguiente comando en tu disco:

```
cd existing_repo
git clone https://gitlab.eif.urjc.es/ruben.alvarez.martin/practicanutricionfinal.git
```

Sin embargo esto solo te hará una copia que internamente tiene referencias a mi proyecto. Si haces cambios no podrás subirlo a tu proyecto. Solo es útil para poder usar los ficheros que os incluyo. NO modifiques ese directorio.